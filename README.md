Launch files, models, and other assets used for simulating
the Raven vehicle.

NB: For now, everything (other than plugins) is going in here;
    we may eventually want to move more generally useful pieces into
    separate repos for simpler code release.

